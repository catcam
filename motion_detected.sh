#!/bin/sh

EMAIL="foo@example.com"

MOVIE="$1"
WEBM_MOVIE="${MOVIE%%.avi}.webm"
MP4_MOVIE="${MOVIE%%.avi}.mp4"
JPG_MOVIE="${MOVIE%%.avi}.jpg"

FILENAME=$(basename "$1")
WEBM_FILE="${FILENAME%%.avi}.webm"
MP4_FILE="${FILENAME%%.avi}.mp4"
JPG_FILE="${FILENAME%%.avi}.jpg"
HTML_FILE="${FILENAME%%.avi}.html"

ffmpeg -i "$MOVIE" -c:v libvpx -crf 30 -b:v 300K -c:a libvorbis "$WEBM_MOVIE" >/dev/null 2>&1;
ffmpeg -i "$MOVIE" -vcodec mpeg4 "$MP4_MOVIE" >/dev/null 2>&1;

LENGTH="$(ffprobe -i "$MOVIE" -show_entries format=duration -v quiet -of csv=p=0)"

ffmpeg -ss 00:00:$(echo 'scale=2;' $LENGTH / 2|bc) -i "$MOVIE" -vframes 1 -q:v 2 "$JPG_MOVIE" >/dev/null 2>&1;

scp -i /home/motion/motionuser_rsa "$WEBM_MOVIE" "$MP4_MOVIE" "$JPG_MOVIE" motionuser@rzlab.ucr.edu:cat_pics/.;
cat <<EOF | ssh -i /home/motion/motionuser_rsa motionuser@rzlab.ucr.edu "cat - > cat_pics/$HTML_FILE";
<html><head><title>Cat Pics $HTML_FILE</title></head>
<body>
<video autoplay controls loop>
<source src="$WEBM_FILE" type="video/webm">
<source src="$MP4_FILE" type="video/mp4">
</video></body></html>
EOF

#cat -<<EOF |mailx -s "Motion detected $FILENAME" "$EMAIL"
#Motion detected on the cat cam!
#
#http://rzlab.ucr.edu/debian/.cat/$HTML_FILE
#EOF
