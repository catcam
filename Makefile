#!/usr/bin/make -f

CAMERA_HOST=catcam.example.com
WEB_HOST=catcam.example.com
WEB_DIR=/srv/catcam/html

-include variables.mk


deploy: deploy-camera deploy-server

deploy-camera: motion_detected.sh
	scp $^ don@$(CAMERA_HOST):.;

deploy-server: index.cgi
	scp $^ don@$(WEB_HOST):$(WEB_DIR)/.;

.PHONY: deploy-camera deploy-server
