#!/usr/bin/perl

use warnings;
use strict;

use CGI::Simple;
use File::Find;
use File::stat;
use File::Basename qw(dirname);
use POSIX;

my $q = CGI::Simple->new({num=>100});

my $cat_dir = dirname($ENV{SCRIPT_FILENAME});


my %pics;
find(sub {
         if (/\.html$/) {
             $pics{$_} = stat($_);
         }
      },
     $cat_dir);

print $q->header(-status => 200);

print <<EOF;
<!DOCTYPE html>
<html>
<head>
<title>Cat Pictures!</title>
</head>
<body>
<header class="header"><span class="title">Cat Pictures!</span></header>

<section class="catpictures">
EOF

my $num = 0;
my $previous_date = '';
for my $pic (sort {$pics{$b}->ctime <=> $pics{$a}->ctime} keys %pics) {
    my $this_date = POSIX::strftime('%Y-%m-%d',localtime($pics{$pic}->ctime()));
    if ($previous_date ne $this_date){
        print "<p>$this_date</p>\n";
        $previous_date = $this_date;
    }
    my $basename = $pic;
    $basename =~ s/\.html//;
    print <<EOF;
<a href="$pic"><img src="${basename}.jpg" width=100 height=100 alt="$basename"></a>
EOF
    $num++;
    last if $num > 30;
}

print <<EOF;
</section>
<footer>
<span class="footer">A silly CGI script</span>
</footer>
</html>
EOF

sub error {
    my ($q,$error,$text) = @_;
    $text //= '';
    print $q->header(-status => $error);
    print "<h2>$error: $text</h2>";
    exit 0;
}
